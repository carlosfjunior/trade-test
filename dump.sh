#!/bin/env bash
#Criterio 1
# Crie um script que faça backup do diretório /etc/ dentro da pasta /.backup/ com o padrão de nome etc-ANOMESDIA-HORAMINUTO.tgz 
#Criterio 2
# e agendar para que este seja executado a cada hora. 
#Criterio 3
#Remova o diretório /TRADE

#set -x

#Variaveis dos diretórios
DirBackup="/.backup"
dirOrig="/etc"
dirRemove="/TRADE"

#Funcao para abortar script
_abort(){
  bufferAppend "❌ - ERROR - $1"
  bufferAppend "❌ - ERROR - Ocorreu um erro durante o script, interromendo execução."
  bufferDisplay
  exit 1
}

#Funcao de dump de pastas
# EX: _dump DiretorioOrigem DiretorioDestino
_dump(){
	dest=$2
	orig=$1
	pattern=$(date +"%Y%m%d-%H%M")
    file="$dest/etc-$pattern.tgz"
    [ ! -d "$orig" ] && _abort "Diretorio $orig nao existe"
    [ ! -d "$dest" ] && _abort "Diretorio $dest nao existe"
    bufferAppend "✅ - Diretorio de origem definido: $orig."
    bufferAppend "✅ - Diretorio de destino definido: $dest."
    bufferAppend "✅ - Iniciando backup de $orig em $file"
	(tar -cvzf  $file $orig 3>&1 /dev/null ) >>/dev/null 2>&1
    [ ! $? -eq 0 ] && _abort "Arquivo compactado com erro"
    [ ! -f "$file" ] && _abort "Erro ao concluir backcup em $file"
    bufferAppend "✅ - Backup Concluido em $file"
    bufferAppend "✅ - Tamanho do arquivo: $(du -hs $file)"
}

#Remove diretorios
_rmDir(){
    local dir=$1
    [ ! -d "$dir" ] && _abort "Diretorio $dir nao existe"
    bufferAppend "✅ - Removendo diretorio $dir"
    rm -rf $dir
    [ ! $? -eq 0 ] && _abort "Erro ao excluir $dir"
}

#Funcao agendador crontab
_schedule(){

    if [[ $EUID -ne 0 ]]; then
        _abort "Para  incluir o agendador você deve estar logado como root."
    fi
	local SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
	local SCRIPT_NAME=${BASH_SOURCE[0]}
    [ -f /etc/cron.d/$SCRIPT_NAME ] && _abort "Crontab ja existe, digite sudo rm /etc/crond.d/$SCRIPT_NAME"
	echo "0 */1 * * * root $SCRIPT_DIR/$SCRIPT_NAME" > /etc/cron.d/$SCRIPT_NAME
	chmod 600 /etc/cron.d/$SCRIPT_NAME
    bufferAppend "✅ - Crontab criado com sucesso"
}

#Buffer erros
bufferReset (){
    unset BUFFERVAR
    BUFFERCURSOR=0
}
bufferAppend () {
    (( BUFFERCURSOR++ ))
    BUFFERVAR[$BUFFERCURSOR]="$1"
}

bufferDisplay () {
    clear  
    for ((i=1;i<=$BUFFERCURSOR;i++));do 
        printf "%s\n" "${BUFFERVAR[$i]}"; 
    done

}

#main =D
main(){
    source /etc/os-release
    OS=$ID
    OS_VERSION=$VERSION_ID
    bufferAppend "🐧 - Script trade iniciado"
    if [[ $ID == "debian" ]];then
        bufferAppend "✅ - OK - Sistema operacional Debian detectado."
    else
        bufferAppend "❌ - ERROR - Sistema operaciona não homologado!"
        bufferDisplay | grep ERROR
    fi
    [ ! ${#schedule} -eq 0 ] &&  _schedule
	_dump $dirOrig $DirBackup
    _rmDir $dirRemove
    bufferDisplay
}
schedule=$(printf "%s\n" "$@" | grep schedule)
main